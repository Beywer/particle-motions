export class ParticleSystemParams {
    public alpha: number = 0;
    public beta: number = 0;
    public speed: number = 0;
    public radius: number = 0;

    constructor(alpha: number = 0, beta: number = 0, speed: number = 0, radius: number = 0) {
        this.alpha = alpha;
        this.beta = beta;
        this.speed = speed;
        this.radius = radius;
    }
}
