import {Particle} from 'model/particles/Particle';
import {ParticleSystemParams} from 'model/particles/ParticleSystemParams';

export class ParticlesSystem {
    public particles: Particle[] = [];
    public params: ParticleSystemParams;

    constructor(particles: Particle[], params: ParticleSystemParams) {
        this.particles = particles;
        this.params = params;
        defineNeighbours(this.particles, this.params);
    }

    public toNext(): ParticlesSystem {
        const updated = updatePositions(this.particles, this.params);
        return new ParticlesSystem(updated, this.params);
    }
}

function defineNeighbours(particles: Particle[], params: ParticleSystemParams): void {
    for (let i = 0; i < particles.length - 1; i++) {
        const p1 = particles[i];
        for (let j = i + 1; j < particles.length; j++) {
            const p2 = particles[j];
            const distance = p1.position.sub(p2.position).size();
            if (distance > params.radius) {
                continue;
            }

            p1.neighbours.push(p2);
            p2.neighbours.push(p1);
        }
    }
}

function updatePositions(particles: Particle[], params: ParticleSystemParams): Particle[] {
    return particles.map((p) => {
        const angleChange = defineAngleChange(p, params);
        const nextOrientation = p.orientation + angleChange;
        const movement = p.direction.mul(params.speed);
        const nextPosition = p.position.add(movement);

        return new Particle(nextPosition, nextOrientation);
    });
}

function defineAngleChange(p: Particle, params: ParticleSystemParams): number {
    return params.alpha + params.beta * p.neighbours.length * Math.sign(p.right - p.left);
}
