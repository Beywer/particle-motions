import {Vector} from 'model/spatial/Vector';
import {nextId} from 'util/nextId';

export class Particle {
    public id: string = nextId();
    public position: Vector;
    public orientation: number;
    public neighbours: Particle[] = [];
    private _direction: Vector = undefined;
    private _right: number = undefined;
    private _left: number = undefined;

    constructor(position: Vector, orientation: number) {
        this.position = position;
        this.orientation = orientation;
    }

    get direction(): Vector {
        if (this._direction === undefined) {
            this._direction = new Vector(
                Math.cos(this.orientation),
                Math.sin(this.orientation),
            );
        }
        return this._direction;
    }

    get right(): number {
        if (this._right === undefined) {
            const lr = getRightsAndLefts(this, this.neighbours);
            this._left = lr.left;
            this._right = lr.right;
        }
        return this._right;
    }

    get left(): number {
        if (this._left === undefined) {
            const lr = getRightsAndLefts(this, this.neighbours);
            this._left = lr.left;
            this._right = lr.right;
        }
        return this._left;
    }
}

function getRightsAndLefts(p: Particle, neighbours: Particle[]): { left: number, right: number } {
    let left = 0;
    let right = 0;
    for (const neighbour of neighbours) {
        if (isRight(p, neighbour)) {
            right++;
        } else {
            left++;
        }
    }
    return {left, right};
}

function isRight(p1: Particle, p2: Particle): boolean {
    const linkVector = p2.position.sub(p1.position);
    const angleDiff = p1.direction.getDiffAngle(linkVector);

    if (angleDiff >= 0) {
        return angleDiff >= 0 && angleDiff <= Math.PI;
    } else {
        return !(angleDiff <= 0 && angleDiff >= -Math.PI);
    }
}
