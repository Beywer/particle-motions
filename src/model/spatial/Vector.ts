export class Vector {
    public x: number = 0;
    public y: number = 0;
    public z: number = 0;
    private _xAngle: number = undefined;

    constructor(x: number, y: number, z: number = 0) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    get xAngle(): number {
        if (this._xAngle !== undefined) {
            return this._xAngle;
        }

        const {x, y} = this;
        if (x === 0) {
            if (y > 0) {
                return Math.PI / 2;
            } else if (y < 0) {
                return -Math.PI / 2;
            } else {
                return 0;
            }
        }
        const base = Math.atan(Math.abs(y / x));

        if (y >= 0) {
            this._xAngle = x > 0 ? base : Math.PI - base;
        } else {
            this._xAngle = x > 0 ? -base : -Math.PI + base;
        }
        return this._xAngle;
    }

    public sub(v: Vector): Vector {
        return new Vector(this.x - v.x, this.y - v.y, this.z - v.z);
    }

    public size(): number {
        const {x, y, z} = this;
        return Math.sqrt(x * x + y * y + z * z);
    }

    public getDiffAngle(v: Vector): number {
        return this.xAngle - v.xAngle;
    }

    public mul(num: number): Vector {
        return new Vector(num * this.x, num * this.y, num * this.z);
    }

    public add(v: Vector): Vector {
        return new Vector(this.x + v.x, this.y + v.y, this.z + v.z);
    }
}
