const radConvertFactor = 2 * Math.PI / 360;

export function toRad(angle: number): number {
    return angle * radConvertFactor;
}
