const prefixes: { [key: string]: number } = {
    '': 0,
};

export function nextId(prefix: string = ''): string {
    prefixes[prefix] = prefixes[prefix] === undefined ? 0 : prefixes[prefix] + 1;
    return `${prefix}_${prefixes[prefix]}`;
}
