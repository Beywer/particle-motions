import {Particle} from 'components/Particle';
import {ParticlesSystem} from 'model/particles/ParticlesSystem';
import * as React from 'react';
import styles from '../styles/components/App.css';

declare interface IAppProps {
    particlesSystem: ParticlesSystem;
}

export class App extends React.Component<IAppProps> {
    public state = {
        particlesSystem: this.props.particlesSystem,
    };

    public componentDidMount(): void {
        setInterval(() => this.setState({
            particlesSystem: this.state.particlesSystem.toNext(),
        }), 20);
    }

    public render() {
        const {particlesSystem} = this.state;

        return (
            <div className={styles.particlesRoot}>
                {particlesSystem.particles.map((p) => <Particle key={p.id} {...p}/>)}
            </div>
        );
    }
}
