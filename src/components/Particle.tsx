import {Particle as ParticleModel} from 'model/particles/Particle';
import {Vector} from 'model/spatial/Vector';
import * as React from 'react';
import styles from 'styles/components/Particle.css';

declare interface IParticleProps {
    position: Vector;
    orientation: number;
    neighbours: ParticleModel[];
}

export function Particle(props: IParticleProps) {
    const left = `calc(50% + ${props.position.x}px)`;
    const top = `calc(50% - ${props.position.y}px)`;
    const color = defineColor(props.neighbours.length);

    return (
        <div
            className={styles.particle}
            style={{left, top, backgroundColor: color}}
        />
    );
}

function defineColor(neighboursCount: number): string {
    if (neighboursCount < 10) {
        return 'green';
    } else if (neighboursCount < 15) {
        return 'cadetblue';
    } else if (neighboursCount < 20) {
        return 'coral';
    }
    return 'rebeccapurple';
}
