import 'assets';
import {App} from 'components/App';
import {Particle} from 'model/particles/Particle';
import {ParticlesSystem} from 'model/particles/ParticlesSystem';
import {ParticleSystemParams} from 'model/particles/ParticleSystemParams';
import {Vector} from 'model/spatial/Vector';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import 'styles/app.css';
import particleStyles from 'styles/components/Particle.css';
import {toRad} from 'util/angles';

const particleSize = parseFloat(particleStyles.size);
const speed = 0.67 * particleSize;
const detectionRadius = 5 * particleSize;

const count = 300;
const sizeX = 700;
const sizeY = 500;

const root = document.getElementById('root');
ReactDOM.render(
    <App particlesSystem={generateParticlesState()}/>,
    root,
);

function generateParticlesState(): ParticlesSystem {
    const params = new ParticleSystemParams(toRad(180), toRad(17), speed, detectionRadius);
    const particles = new Array(count)
        .fill(1)
        .map(() => new Particle(
            new Vector(Math.random() * sizeX - sizeX / 2, Math.random() * sizeY - sizeY / 2),
            Math.random() * 2 * Math.PI,
        ));

    return new ParticlesSystem(particles, params);
}
